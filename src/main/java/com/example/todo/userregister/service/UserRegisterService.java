package com.example.todo.userregister.service;


import com.example.todo.userregister.entity.UserRegister;
import com.example.todo.userregister.repository.UserRegisterRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserRegisterService  {
    @Autowired
    private UserRegisterRepo userRegisterRepo;

    public List<UserRegister> findUserRegister() {
        return userRegisterRepo.findAll();
    }


    public UserRegister save(UserRegister register) {
        return userRegisterRepo.save(register);
    }
}
