package com.example.todo.userregister.controller;

import com.example.todo.userregister.entity.UserRegister;
import com.example.todo.userregister.service.UserRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserRegisterController {
    @Autowired
    private UserRegisterService userRegisterService;

    @GetMapping("/register")
    public String UserList(ModelMap map) {
        map.put("registers", userRegisterService.findUserRegister());
        return "register";

    }

    @PostMapping("/register")
    public String UserRegistered(@ModelAttribute UserRegister register) {
      // String result = select user from user_register where user=register.getUser();
       //if there is something in result show error else show success
        //html select option

        userRegisterService.save(register);
        return "welcome";
    }


}
