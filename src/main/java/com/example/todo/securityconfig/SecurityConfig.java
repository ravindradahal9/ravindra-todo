package com.example.todo.securityconfig;


import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;




@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //setting own configuration on the auth
        auth.inMemoryAuthentication()
                .withUser("Ravi")
                .password("Ravi")
                .roles("ADMIN")
                .and()
                .withUser("Ravindra")
                .password("Ravindra")
                .roles("USER");
    }
    @Bean
    public PasswordEncoder getPassWord(){

        return NoOpPasswordEncoder.getInstance();
    }
//getInstance();
    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests().anyRequest().authenticated().and()
//                .formLogin().defaultSuccessUrl("/",true)
//                .and().logout().logoutSuccessUrl("/logout");

        http.authorizeRequests()
                .antMatchers("/admin").hasRole("ADMIN")
                .antMatchers("/user").hasAnyRole("USER","ADMIN")
                .antMatchers("/").permitAll()
                .and().formLogin().and().logout();
    }

}
