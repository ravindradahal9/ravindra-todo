package com.example.todo.user.repository;

import com.example.todo.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User,Integer > {



}

