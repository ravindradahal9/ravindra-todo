package com.example.todo.user.controller;


import com.example.todo.user.entity.User;
import com.example.todo.userregister.entity.UserRegister;
import com.example.todo.user.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;


@Controller
public class UserController {

    public final UserService userService;


    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/")
    public String getList(ModelMap map) {
        map.put("users", userService.findUsers());
        return "welcome";

    }

    @PostMapping("/")
    public String register(@ModelAttribute User user) {
        userService.save(user);
        return "redirect:/";

    }



    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable int id) {
        userService.deleteUserById(id);
        return "redirect:/";

    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editUser(ModelMap map, @PathVariable int id) {
        map.put("users", userService.getUserById(id));
        return "edit";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editSave(@ModelAttribute("user") User user) {
        if (user.getId() != null)
            userService.save(user);
        return "redirect:/";
    }

}