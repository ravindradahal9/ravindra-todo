package com.example.todo.user.service;

import com.example.todo.user.entity.User;
import com.example.todo.userregister.entity.UserRegister;

import java.util.List;

public interface UserService<id> {

    List<User> findUsers();

    User save(User user);

    User getUserById(int id);


    String deleteUserById(int id);

    User editUserById(User user, int id);


    User get(int id);

}
