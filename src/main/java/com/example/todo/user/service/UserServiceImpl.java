package com.example.todo.user.service;


import com.example.todo.user.entity.User;
import com.example.todo.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;


    @Override
    //  get method
    // for all the users
    public List<User> findUsers() {
        return userRepository.findAll();
    }

    @Override
    // for the single user to save
    public User save(User user) {
        return userRepository.save(user);
    }


    @Override
    //get method by id
    public User getUserById(int id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public String deleteUserById(int id) {
        userRepository.deleteById(id);
        return "user removed  " + id;
    }


    @Override
    public User editUserById(User user, int id) {
        User user1 = userRepository.findById(user.getId()).orElse(null);
        user1.setName(user.getName());
        user1.setEmail(user.getEmail());
        user1.setList(user.getList());
        return userRepository.save(user1);
    }

    @Override
    public User get(int id) {
        return userRepository.findById(id).orElse(null);
    }

}
