package com.example.todo.webservice;


import com.example.todo.user.entity.User;
import com.example.todo.user.service.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user/v1")
@AllArgsConstructor
public  class UserRestController {

    private final UserServiceImpl userService;

    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers() {
        List<User> users = null;
        ResponseDto responseDto = new ResponseDto();
        try {
            users = userService.findUsers();
            responseDto.setMessage("successfully fetched data");
            responseDto.setSuccess(true);
            responseDto.setData(users);
        } catch (Exception e) {
            responseDto.setMessage("failed to fetched data");
            responseDto.setSuccess(false);


        }
        return ResponseEntity.ok(responseDto);
    }
    @PostMapping("/users")
    public ResponseEntity<?> getSavedUsers(@RequestBody User user) {
        ResponseDto responseDto = new ResponseDto();
        try {
            user = userService.save(user);
            responseDto.setMessage("successfully saved data");
            responseDto.setSuccess(true);
            responseDto.setData(user);
        } catch (Exception e) {
            responseDto.setMessage("failed to fetched data");
            responseDto.setSuccess(false);


        }
        return ResponseEntity.ok(responseDto);
    }
    @PutMapping("/users")
    public ResponseEntity<?> updateUsers(@RequestBody User user) {
        ResponseDto responseDto = new ResponseDto();
        try {
            user = userService.save(user);
            responseDto.setMessage("User has been successfully updated date");
            responseDto.setSuccess(true);
            responseDto.setData(user);
        } catch (Exception e) {
            responseDto.setMessage("failed to update data");
            responseDto.setSuccess(false);


        }
        return ResponseEntity.ok(responseDto);
    }
    @DeleteMapping("/users")
    public ResponseEntity<?> deleteUsers(@RequestParam int id) {
        ResponseDto responseDto = new ResponseDto();
        try {
       userService.deleteUserById(id);
            responseDto.setMessage("User has been successfully deleted ");
            responseDto.setSuccess(true);

        } catch (Exception e) {
            responseDto.setMessage("failed to delete data");
            responseDto.setSuccess(false);


        }
        return ResponseEntity.ok(responseDto);
    }


}
