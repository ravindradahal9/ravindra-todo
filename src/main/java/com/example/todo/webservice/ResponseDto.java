package com.example.todo.webservice;

import lombok.Data;

@Data
public class ResponseDto {
    private String message;
    private boolean success;
    private Object data;
}
